import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';

import { EditprofilePage } from '../editprofile/editprofile';
import { AuthService  } from '../../providers/authservice/authservice';
import { HomePage } from '../home/home';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-change-password',
  templateUrl: 'changepassword.html',
})
export class ChangePasswordPage {

	resposeData : any;
	public currentUser:any;
	userData = {"useremail":"", "password":"", "newpassword":""};
	
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
		const data = JSON.parse(localStorage.getItem('userData'));
		this.currentUser = data.userData;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
	
	changepass(){
		this.presentLoading();
		if(this.userData.useremail && this.userData.password && this.userData.newpassword && (this.userData.password != this.userData.newpassword)){
			this.authService.postData(this.userData, "changepassword").then((result) =>{
			this.resposeData = result;
			if(this.resposeData.userData){
				console.log(this.resposeData.userData);
				this.presentToast("Password Updated successfully.");
				this.navCtrl.push(EditprofilePage);
			}
			else{
				this.showAlert();
			}
			}, (err) => {
				//Connection failed message
			});
		}
		else{
			this.showAlert();
		}
	  
	}

	presentLoading() {
		const loader = this.loadingCtrl.create({
		  spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon',
		  duration: 3000
		});
		loader.present();
	}


	showAlert() {
		const alert = this.alertCtrl.create({
		  title: 'Incorrect!',
		  subTitle: 'Username and Password',
		  buttons: ['OK']
		});
		alert.present();
	}
	
	presentToast(msg) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: 6000,
			position: 'bottom'
		});
		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}
	
	backToProfile(){
		this.navCtrl.push(EditprofilePage);
	}
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }
}
