import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController, ActionSheetController, Platform } from 'ionic-angular';

import { AuthService } from '../../providers/authservice/authservice';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { MediaCapture, MediaFile, CaptureVideoOptions } from '@ionic-native/media-capture';

import { Login } from '../login/login';
import { RegisterPage } from '../register/register';
import { FavteamPage } from '../favteam/favteam';
import { ProfilePage } from '../profile/profile';
import { CommunityPage } from '../community/community';
import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';
import { FriendprofilePage } from '../friendprofile/friendprofile';
import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview'; 
import { FanrankingPage } from '../fanranking/fanranking'; 
import { ExplorePage } from '../explore/explore';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Http } from '@angular/http';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	@ViewChild('fileInput') fileInput;
    public resposeData: any;
	public imageFileName: any;
	public tagFriendsData: any;
    public locationlistData: any;
    public userDetails: any;
	public latestPosts: string[] = [];
	userProfileData = {
		profilePicture: "",
		userName: "",
		userFollowers: "",
		userFriends: "",
		userFollowersCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
		url: "",
		type: "",
		triviaGame: "",
		gameStatus: "",
		gameMessage: "",
	};
	userPostData = {
		user_id: "", 
		feed_id: "",
		feed: "",
		mentionFriends: "",
		cmnt_parentID: 0,
		page: 1,
		perPage: 15,
		totalData: 0,
		totalPage: 0,
	};
	gamePostData = {
		selectedOption: "",
		user_id: "",
		game_id: "",
	};
	loader: any;
	videoId: any;
	flag_upload = true;
	flag_play = true;
	public selectedWebFiles: File = null;

  constructor(public navCtrl: NavController, public navParams: NavParams,public loadingCtrl: LoadingController,public authService:AuthService,public camera : Camera,public transfer: FileTransfer,public toastCtrl: ToastController, public alertCtrl: AlertController,private mediaCapture: MediaCapture, public socialSharing: SocialSharing, public actionsheetCtrl: ActionSheetController, public platform: Platform, public http: Http) {
    const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
	this.userPostData.user_id = this.userDetails.user_id;
	this.gamePostData.user_id = this.userDetails.user_id;
	this.getUserProfileData();
	
  }

  getUserProfileData() {
	let loading = this.loadingCtrl.create({
		spinner: 'hide',
		content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
		cssClass: 'football_icon'
	});
	loading.present();
	this.authService.postData(this.userPostData, "getuserprofiledata").then(
	result => {
	  this.resposeData = result;
	  if (this.resposeData.profileData) {
		loading.dismiss();
		/*this.dataSet = this.resposeData.feedData;
		const dataLength = this.resposeData.feedData.length;
		this.userPostData.lastCreated = this.resposeData.feedData[
		  dataLength - 1
		].created;*/
		this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
		this.userProfileData.userName = this.resposeData.profileData.userName;
		this.userProfileData.userFollowers = this.resposeData.profileData.userFollowers;
		this.userProfileData.userFriends = this.resposeData.profileData.userFriends;
		this.userProfileData.userFollowersCount = this.resposeData.profileData.userFollowersCount;
		this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
		for(let i=0; i<this.resposeData.profileData.latestPost.length; i++) {
			this.latestPosts.push(this.resposeData.profileData.latestPost[i]);
		}
		//this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
		this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
		this.userProfileData.triviaGame = this.resposeData.profileData.triviaGame;
		this.gamePostData.game_id = this.resposeData.profileData.triviaGame[0].id;
		this.userProfileData.gameStatus = this.resposeData.profileData.gameStatus;
		this.userProfileData.gameMessage = "You already played this game.";
		
		
		this.userPostData.page = this.resposeData.profileData.latestPost.page;
		this.userPostData.totalData = this.resposeData.profileData.latestPost.totalData;
		this.userPostData.totalPage = this.resposeData.profileData.latestPost.totalPage;
		
	  } else {
		console.log("No access");
		loading.dismiss();
	  }
	},
	err => {
	  //Connection failed message
	  loading.dismiss();
	}
  );
}


  /* Upload Images/Videos for user post starts */
	uploadPostimgvideo(){
		if (Camera['installed']()) {
		const options : CameraOptions = {
			quality: 100, // picture quality
			mediaType: this.camera.MediaType.ALLMEDIA,
			destinationType : this.camera.DestinationType.FILE_URI,
			sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit: true,
		}
		
		this.camera.getPicture(options) .then((imageData) => {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			/* Starts Preloader */
			this.imageFileName = imageData.substring(imageData.lastIndexOf('/')+1);
			const fileTransfer: FileTransferObject = this.transfer.create();
			let options11: FileUploadOptions = {
				fileKey: 'file',
				fileName: this.imageFileName,
				params : {"userData":this.userDetails.user_id},
				headers: {}
			}
			fileTransfer.upload(imageData, 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/postsUpload.php', options11, true).then((data) => {
				/* success
				/alert("success"); */
				loading.dismiss();
				this.resposeData = data;
				let mediaresponse = JSON.parse(this.resposeData.response);
				this.authService.postData(mediaresponse, "getreturndata").then(
					result => {
						this.resposeData = result;
						if(this.resposeData.postType != ""){
							this.userProfileData.postMedia = this.resposeData.postMedia;
							this.userProfileData.postType = this.resposeData.postType;
							this.presentToast("Upload Success");
						}
						else{
							this.presentToast("There was an error uploading the file, please try again!");
						}
					},
					err => {
						this.presentToast("Returned Error");
					}
				);	
			}, (err) => {
				loading.present();
				this.presentToast(JSON.stringify(err));
			});
		}, (err) => {
		  console.log(err);
		});
		}else {
			this.fileInput.nativeElement.click();
		}
	}
    /* Upload Images/Videos for user post Ends */
    
    /* Upload user profile pic Starts */
	takePhoto() {
		if (Camera['installed']()) {
		const options : CameraOptions = {
			quality: 100, // picture quality
			mediaType: this.camera.MediaType.PICTURE,
			// sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit: true,
		}
		/*let options = {
			quality: 100,
			mediaType: this.camera.MediaType.PICTURE,
		};*/
		this.camera.getPicture(options) .then((imageData) => {
		  /*this.base64Image = "data:image/jpeg;base64," + imageData;
		  this.photos.push(this.base64Image);
		  this.photos.reverse();*/
		  
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			/* Starts Preloader */
			/*this.presentToast(imageData);*/
			this.imageFileName = imageData.substring(imageData.lastIndexOf('/')+1);
			const fileTransfer: FileTransferObject = this.transfer.create();
			let options11: FileUploadOptions = {
				fileKey: 'file',
				fileName: this.imageFileName,
				params : {"userData":this.userDetails.user_id},
				headers: {}
			}
			fileTransfer.upload(imageData, 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/postsUpload.php', options11, true).then((data) => {
				/* success
				/alert("success"); */
				loading.dismiss();
				this.resposeData = data;
				let mediaresponse = JSON.parse(this.resposeData.response);
				this.authService.postData(mediaresponse, "getreturndata").then(
					result => {
						this.resposeData = result;
						if(this.resposeData.postType != ""){
							this.userProfileData.postMedia = this.resposeData.postMedia;
							this.userProfileData.postType = this.resposeData.postType;
							this.presentToast("Upload Success");
						}
						else{
							this.presentToast("There was an error uploading the file, please try again!");
						}
					},
					err => {
						this.presentToast("Returned Error");
					}
				);	
			}, (err) => {
				// error
				//alert("error"+JSON.stringify(err));
				loading.dismiss();
				this.presentToast(JSON.stringify(err));
			});
		}, (err) => {
		  console.log(err);
		});
		}else {
			this.fileInput.nativeElement.click();
		}
	}
	/* Upload user profile pic Ends */
	processWebImage(event){
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.selectedWebFiles = <File>event.target.files[0];
		const fd = new FormData();
		fd.append("file", this.selectedWebFiles, this.selectedWebFiles.name);
		fd.append("userData", this.userDetails.user_id);
		this.http.post('http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/webPostUpload.php', fd).map(res => res.json()).subscribe(data => {
			loading.dismiss();
			this.resposeData = data;
			if(this.resposeData.postType != ""){
				this.userProfileData.postMedia = this.resposeData.postMedia;
				this.userProfileData.postType = this.resposeData.postType;
				this.presentToast("Upload Success");
				event.target.value = "";
			}
			else{
				this.presentToast("There was an error uploading the file, please try again!");
			}
		}, error => {
			loading.dismiss();
			console.log(error);
		});
		
	}
	/* Create User Post Starts */
	createPost(){
		let nxtpage:any;
		nxtpage = this.userPostData.page - parseInt("1");
		this.resposeData = {
			user_id: this.userPostData.user_id,
			postMedia : this.userProfileData.postMedia,
			postText: this.userProfileData.postText,
			postType: this.userProfileData.postType,
			team_id: this.userDetails.team_id,
			url: this.userProfileData.postMedia,
			postTags: this.userProfileData.postTags,
			postChallengeType: 'social',
			page: 1,
			perPage: nxtpage*15,
		};
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		/*if (this.userProfileData.postText) {*/
		loading.present();
		this.authService.postData(this.resposeData, "createpost").then(
			result => {
				this.resposeData = result;
				loading.dismiss();
				if (this.resposeData.error <= 0) {
					this.presentToast(this.resposeData.message);
					/*this.userProfileData.latestPost = this.resposeData.data;*/
					this.latestPosts = [];
					for(let i=0; i<this.resposeData.data.length; i++) {
						this.latestPosts.push(this.resposeData.data[i]);
					}
					this.userPostData.page = parseInt(nxtpage) + parseInt("1");
					this.userPostData.perPage = 15;
					this.userProfileData.postText = "";
					this.userProfileData.postType = "";
					this.userProfileData.postMedia = "";
					this.userProfileData.postTags = "";
					
				} else {
					console.log("No access");
					
					this.presentToast(this.resposeData.message);
				}
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			}
		);
		/*}*/
	}
	/* Create User Post Ends */

	removePhoto(){
		this.userProfileData.postText = "";
		this.userProfileData.postType = "";
		this.userProfileData.postMedia = "";
	}

	/* Delete User Post Starts */
	deletePost(index, ID) {
		let confirm = this.alertCtrl.create({
			title: 'Are you sure, you want to delete this post? There is NO undo!',
			message: '',
			buttons: [
			{
			  text: 'No',
			  handler: () => {
				console.log('Disagree clicked');
			  }
			}, {
			text: 'Yes',
			handler: () => {
				let loading = this.loadingCtrl.create({
					spinner: 'hide',
					content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
					cssClass: 'football_icon'
				});
				loading.present();
				this.userPostData.feed_id = ID;
				let nxtpage:any;
				nxtpage = this.userPostData.page - parseInt("1");
				this.userPostData.page = 1;
				this.userPostData.perPage = nxtpage*15;
				this.authService.postData(this.userPostData, "deletepost").then(
					result => {
						this.resposeData = result;
						loading.dismiss();
						if (this.resposeData.error <= 0) {
							/*this.userProfileData.latestPost = this.resposeData.data;*/
							this.latestPosts = [];
							for(let i=0; i<this.resposeData.data.length; i++) {
								this.latestPosts.push(this.resposeData.data[i]);
							}
							this.userPostData.page = parseInt(nxtpage) + parseInt("1");
							this.userPostData.perPage = 15;
							this.presentToast("Post Deleted Successfully!");
						}  else {
							this.presentToast("Error occurs while deleting post.");
						}
					},
					err => {
					  //Connection failed message
					  loading.dismiss();
					  this.presentToast("Error occurs while deleting post.");
					}
				);
			}
			}
		  ]
		});
	  confirm.present();
	}
	/* Delete User Post Ends */
	tagFriends() {
		let alert = this.alertCtrl.create();
		alert.setTitle('Please Select Your Friends');
		let followersData = this.userProfileData.userFriends;
		Object.keys(followersData).forEach( function(key) {
			console.log(followersData[key].username);
			alert.addInput({
				type: 'checkbox',
				label: followersData[key].username,
				value: followersData[key].username,
			});
			
		});
		alert.addButton('Cancel');
		alert.addButton({
			text: 'Okay',
			handler: (data: any) => {
				this.tagFriendsData = "";
				data.forEach( d => {
					this.tagFriendsData  = this.tagFriendsData+d+",";
				});
				this.tagFriendsData = this.tagFriendsData;
				this.userProfileData.postTags = this.tagFriendsData;
			}
		});

		alert.present();
	}

	mentionFriends($event, newComment) {
		let alert = this.alertCtrl.create();
		alert.setTitle('Please Select Your Friends');
		let followersData = this.userProfileData.userFriends;
		Object.keys(followersData).forEach( function(key) {
			alert.addInput({
				type: 'checkbox',
				label: followersData[key].username,
				value: followersData[key],
			});
			
		});
		alert.addButton('Cancel');
		alert.addButton({
			text: 'Okay',
			handler: (data: any) => {
				this.userPostData.mentionFriends = "";
				data.forEach( d => {
					$event.target.value  = $event.target.value+" "+d.username+" ";
					this.userPostData.mentionFriends = this.userPostData.mentionFriends+d.friend_id+",";
				});
				$event.target.value = $event.target.value.replace(/@(.*?) /i,"");
				newComment.value = $event.target.value;
			}
		});

		alert.present();
	}

	tvgAnswer(selectedOption){
		this.gamePostData.selectedOption = selectedOption;
	}
	submitGame(){
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Submitting, Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.gamePostData, "submittriviagame").then(
			result => {
				this.resposeData = result;
				if(this.resposeData.message){
					this.userProfileData.gameMessage = this.resposeData.message;
					let triviagameBgEl = document.querySelector('.triviagameBgParent');
					let boxtitleEl = document.querySelector('.boxtitle');
					triviagameBgEl.classList.add('triviagamedisplay');
					boxtitleEl.classList.add('showboxtitle');
					this.presentToast(this.userProfileData.gameMessage);
					this.navCtrl.push(HomePage);
				}
				loading.dismiss();
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			}
		);
		
	}

	likePhoto(index, ID) {
		this.userPostData.feed_id = ID;
		this.authService.postData(this.userPostData, "likepost").then(
			result => {
				this.resposeData = result;
				if (this.resposeData.error <= 0) {
					console.log(this.resposeData.message);
				} else {
					console.log("Error occurs while submitting like.");
				}
			},
			err => {
			  this.presentToast("Error occurs while submitting like.");
			}
		);
		
		let postCount = this.userPostData.perPage * (this.userPostData.page - 1);
		for(let i=0; i<postCount; i++) {
			if(index == i){
				let sp = this.latestPosts[i];
				let lstatus = 0;
				Object.keys(sp).forEach(function(key) {
					if(key == "likesStatus" && sp["likesStatus"] < 1 ){
						sp["likes"] = parseInt(sp["likes"]) + parseInt("1");
						sp["likesStatus"] = 1;
						lstatus = 1;
					}else if(key == "likesStatus"){
						sp["likes"] = parseInt(sp["likes"]) - parseInt("1");
						sp["likesStatus"] = 0;
					}
				});
				if(lstatus > 0){
					this.presentToast("Like submitted successfully!");
				}else{
					this.presentToast("Unlike submitted successfully!");
				}
				this.latestPosts[i] = sp;
			}
		}
		
	}

	sharePhoto(ID) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.userPostData.feed_id = ID;
		this.authService.postData(this.userPostData, "sharepost").then(
			result => {
				this.resposeData = result;
				loading.dismiss();
				if (this.resposeData.error == 0) {
					this.presentToast("Shared Successfully!");
					this.navCtrl.push(HomePage);
				} else {
					this.presentToast("Error occurs while shareing post.");
				}
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			  this.presentToast("Error occurs while shareing post.");
			}
		);
			  
	}
	
	comments = [];
	addComment(index, ID, newComment: string) {
		if (newComment) {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			this.userPostData.feed_id = ID;
			this.userPostData.feed = newComment;
			let nxtpage:any;
			nxtpage = this.userPostData.page - parseInt("1");
			this.userPostData.perPage = nxtpage*15;
			this.userPostData.page = 1;
			
			this.authService.postData(this.userPostData, "addpostcomment").then(
				result => {
					loading.dismiss();
					this.resposeData = result;
					if (this.resposeData.error <= 0) {
						this.latestPosts = [];
						for(let i=0; i<this.resposeData.latestPosts.length; i++) {
							this.latestPosts.push(this.resposeData.latestPosts[i]);
						}
						this.userPostData.page = parseInt(nxtpage) + parseInt("1");
						this.userPostData.perPage = 15;
						this.presentToast("Comment added successfully!");
					} else {
						this.presentToast("Error occurs while adding post comment.");
					}
				},
				err => {
				  loading.dismiss();
				  this.presentToast("Error occurs while adding post comment.");
				}
			);
		}
	}

	addCommentReply(comment_id, ID,newReply: string) {
		if (newReply) {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			this.userPostData.cmnt_parentID = comment_id;
			this.userPostData.feed_id = ID;
			this.userPostData.feed = newReply;
			let nxtpage:any;
			nxtpage = this.userPostData.page - parseInt("1");
			this.userPostData.page = 1;
			this.userPostData.perPage = nxtpage*15;
			this.authService.postData(this.userPostData, "addpostcomment").then(
				result => {
					loading.dismiss();
					this.resposeData = result;
					if (this.resposeData.error <= 0) {
						/*this.userProfileData.latestPost = this.resposeData.latestPosts;*/
						this.latestPosts = []
						for(let i=0; i<this.resposeData.latestPosts.length; i++) {
							this.latestPosts.push(this.resposeData.latestPosts[i]);
						}
						this.userPostData.page = parseInt(nxtpage) + parseInt("1");
						this.userPostData.perPage = 15;
						this.presentToast("Replied successfully!");
						/*this.navCtrl.push(HomePage);*/
					} else {
						this.presentToast("Error occurs while adding post comment.");
					}
				},
				err => {
				  //Connection failed message
				  loading.dismiss();
				  this.presentToast("Error occurs while adding post comment.");
				}
			);
		} 
	}


	deletecomnt(index, cindex, commnt_id) {
		/* this.heroes.splice($commnt, 1); */
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		/*this.userProfileData.latestPost.comments.splice(this.userProfileData.latestPost.comments.indexOf(index),1);*/
		this.userPostData.feed_id = commnt_id;
		let nxtpage:any;
		nxtpage = this.userPostData.page - parseInt("1");
		this.userPostData.page = 1;
		this.userPostData.perPage = nxtpage*15;
		this.authService.postData(this.userPostData, "deletepostcomment").then(
			result => {
				loading.dismiss();
				this.resposeData = result;
				if (this.resposeData.message) {
					/*this.userProfileData.latestPost = this.resposeData.latestPosts;*/
					this.latestPosts = []
					for(let i=0; i<this.resposeData.latestPosts.length; i++) {
						this.latestPosts.push(this.resposeData.latestPosts[i]);
					}
					this.userPostData.page = parseInt(nxtpage) + parseInt("1");
					this.userPostData.perPage = 15;
					this.presentToast("Replied successfully!");
					this.presentToast("Comment deleted successfully!");
				} else {
					this.presentToast("Error occurs while adding post comment.");
				}
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			  this.presentToast("Error occurs while adding post comment.");
			}
		);
	}

	videoCaptureFn(){
		let options: CaptureVideoOptions = { limit: 1 };
		this.mediaCapture.captureVideo(options)
		.then((videodata: MediaFile[]) => {
		var i, path, len;
		for (i = 0, len = videodata.length; i < len; i += 1) {
		path = videodata[i].fullPath;
		// do something interesting with the file
		}
		this.videoId = path;
		this.presentToast(this.videoId);
		this.flag_play = false;
		this.flag_upload = false;
		});
	}

	sharePostImage() {
		//this code is to use the social sharing plugin
		// message, subject, file, url
		this.socialSharing.share("Check this item:  appv2://home/")
		.then(() => {
			/*this.presentToast('testing');*/
			
		})
		.catch(() => {
	
		});
	}
	
	webSocialShareFn(id,url,element) {
		let actionSheet = this.actionsheetCtrl.create({
		  title: 'Share on Social Network',
		  cssClass: 'action-sheets-basic-page',
		  buttons: [
			{
			  text: 'Facebook',
			  role: 'share on facebook',
			  icon: !this.platform.is('ios') ? 'logo-facebook' : null,
			  handler: () => {
				this.shareOnFacebook(id,url);
			  }
			},
			{
			  text: 'Twitter',
			  role: 'share on twitter',
			  icon: !this.platform.is('ios') ? 'logo-twitter' : null,
			  handler: () => {
				this.shareOnTwitter(id);
			  }
			},
			{
			  text: 'Pinterest',
			  role: 'share on pinterest',
			  icon: !this.platform.is('ios') ? 'logo-pinterest' : null,
			  handler: () => {
				this.shareOnPinterest(id);
			  }
			},
			{
			  text: 'Email',
			  role: 'share on email',
			  icon: !this.platform.is('ios') ? 'mail' : null,
			  handler: () => {
				this.shareOnEmail(id);
			  }
			},
			{
			  text: 'Copy Link - http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+'',
			  role: 'Copy link',
			  icon: !this.platform.is('ios') ? 'ios-copy' : null,
			  cssClass: 'copyLinkTxt',
			  handler: () => {
			
				this.copyToClipboard(element);
				
			  }
			} 
		  ]
		});
		actionSheet.present();
	}
	
	shareOnFacebook(id,url) {
		window.open('https://www.facebook.com/dialog/feed?app_id= 490648591450536 &redirect_uri=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+' &link='+url+' &picture=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+' &caption=This%20is%20the%20caption &description=This%20is%20the%20description');
	}

	shareOnTwitter(id) {
		window.open('https://twitter.com/intent/tweet?url=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}

	shareOnPinterest(id) {
		window.open('http://pinterest.com/pin/create/link/?url=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}
	
	shareOnEmail(id) {
		window.open('https://mail.google.com/mail/?view=cm&fs=1&to=&su=SFC Post Link&body='+ 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}
	
	copyToClipboard(element) {
		element.select();
		document.execCommand('copy');
		this.presentToast("Link has been copied!");
	}

	showReplies($event){
		let parentClass = $event.target.previousElementSibling.parentNode.parentNode.parentNode.className;
		let repliesEls = document.querySelectorAll('.'+parentClass+' li ul .replydeactive');
		let rstart;
		for (rstart = 0; rstart < repliesEls.length; rstart++) {
			repliesEls[rstart].classList.add('replyactive');
		}
		let activerepliesEls = document.querySelectorAll('.'+parentClass+' li ul .replydeactive.replyactive');
		for (rstart = 0; rstart < activerepliesEls.length; rstart++) {
			activerepliesEls[rstart].classList.remove('replydeactive');
		}
		$event.target.classList.remove('showmore');
		$event.target.classList.add('btndeactive');
	}
	
	showComments($event){
		let parentClass = $event.target.previousElementSibling.className;
		let commentsEls = document.querySelectorAll('.'+parentClass+' li.commentdeactive');
		let start;
		for (start = 0; start < commentsEls.length; start++) {
			commentsEls[start].classList.add('commentactive');
		}
		let activecommentsEls = document.querySelectorAll('.'+parentClass+' li.commentdeactive.commentactive');
		for (start = 0; start < activecommentsEls.length; start++) {
			activecommentsEls[start].classList.remove('commentdeactive');
		}
		$event.target.classList.remove('showmore');
		$event.target.classList.add('btndeactive');
	}
	
    presentToast(msg) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: 6000,
			// position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}
	
	loginPage()
    {
        this.navCtrl.push(Login);
    }
	
	signupPage()
    {
        this.navCtrl.push(RegisterPage);
    }
	
	goToFavteamPage()
    {
        this.navCtrl.push(FavteamPage);
    }
	
	goToProfilePage()
    {
        this.navCtrl.push(ProfilePage);
    }
	
	goToCommunityPage()
    {
        this.navCtrl.push(CommunityPage);
    }
	
	goToSuperFanChallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }
	
	goToFriendprofilePage(user_id)
    {
		this.navCtrl.push(FriendprofilePage,{ userId: user_id });
    }
	
	goToOtherfanentryviewPage(id)
    {
        this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
    }
	
	goToFanrankingPage()
    {
        this.navCtrl.push(FanrankingPage); 
		}

		goToHomePage()
    {
        this.navCtrl.push(HomePage);
    }

    goToexploreScreen()
    {
        this.navCtrl.push(ExplorePage);
    }

    goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

    goTofanRankingPage()
    {
        this.navCtrl.push(FanrankingPage);
    }

    goToprofilePage()
    {
        this.navCtrl.push(ProfilePage);
    }
		
	doInfinite(infiniteScroll) {
		setTimeout(() => {
			this.getUserProfileData();
			console.log('Async operation has ended');
			infiniteScroll.complete();
		}, 1000);
	}	
}
