import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

import { AuthService  } from '../../providers/authservice/authservice';
import { DailycompeteviewPage } from '../dailycompeteview/dailycompeteview';
import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview';
import { CompetescreenPage } from '../competescreen/competescreen';

import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { ExplorePage } from '../explore/explore';
import { FanrankingPage } from '../fanranking/fanranking'; 

/**
 * Generated class for the SuperfanchallengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-superfanchallenge',
  templateUrl: 'superfanchallenge.html',
})
export class SuperfanchallengePage {
	public userDetails: any;
	public posts: any;
	public userPostData = {
		user_id: "",
		challenge_type: "'bonus','superfan'",
		expiredate:"",
		nowdate:new Date().getDay()
	};
	users: any; 
	resposeData : any;
	public bonusdata = {id:"",title:""};
	public superchallengedata = {id:"",title:""};
	public responsebonusData : any;
	public responsesuperchallengeData : any;
	public expiredate:any;
	countDownDate : any;
	timeLeft : any;
	now : any;
	distance : any;
	days : any;
	hours : any;
	minutes : any;
	seconds : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public authService: AuthService,public toastCtrl: ToastController) {
	const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
		this.userPostData.user_id = this.userDetails.user_id;
		this.expiredate = this.userDetails.expiredate;
		console.log(this.expiredate);
		//this.countDownDate = new Date(this.expiredate).getTime();
		//this.countDownDate = Date.parse(this.expiredate.replace(/\s+/g, 'T'));
		//this.countDownDate = new Date(this.expiredate.replace(/\s+/g, 'T').concat('.000+05:30')).getTime();
		this.countDownDate = new Date(this.expiredate).getTime();
		this.getsuperchallengedata();
		this.getbonusdata();
		this.ionViewDidLoad();
  }

  ionViewDidLoad() {
		console.log('ionViewDidLoad SuperfanchallengePage');
		setInterval(() => {
			/*if(navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0){
				var today = new Date();
				today.setHours(today.getHours() + 7);
				this.now = today.getTime();
			}else{
				this.now = new Date().getTime();
			}*/
			this.now = new Date().getTime();
		 // Find the distance between now and the count down date
		 this.distance = this.countDownDate - this.now;
 
		 // Time calculations for days, hours, minutes and seconds
		 this.days = Math.floor(this.distance / (1000 * 60 * 60 * 24));
		 this.hours = Math.floor((this.distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		 this.minutes = Math.floor((this.distance % (1000 * 60 * 60)) / (1000 * 60));
		 this.seconds = Math.floor((this.distance % (1000 * 60)) / 1000);
 
		 // Display the result in the element with id="demo"
		 this.timeLeft =  this.days + "D : " + this.hours + "H : "
		 + this.minutes + "M : " + this.seconds + "S ";
 
		 // If the count down is finished, write some text
		 if (this.distance < 0) {
		 //clearInterval(x);
		 this.timeLeft = "EXPIRED";
		 }
		 
	 }, 1000);
	this.authService.postData(this.userPostData, "getchallengeposts").then(
		result => {
			this.resposeData = result;
		  if (this.resposeData.data) {
			this.posts = this.resposeData.data;
			//console.log(this.posts);
		  }else {
			console.log("No access");
		  }
		},
		err => {
		  //Connection failed message
		  console.log("No access");
		}
	);
	}
	getsuperchallengedata()
	{
	this.authService.postData(this.userPostData, "getsuperchallengedata").then(
		result => {
		  this.responsesuperchallengeData = result;
		  if (this.responsesuperchallengeData.superchallengeData) {
			this.superchallengedata.id = this.responsesuperchallengeData.superchallengeData['id'];
			this.superchallengedata.title = this.responsesuperchallengeData.superchallengeData['title'];
			//this.superchallengedata.expiredate = this.responsesuperchallengeData.superchallengeData['expiredate'];
			 console.log(this.responsesuperchallengeData.superchallengeData);
			 //console.log(this.superchallengedata.expiredate);
		  } else {
			console.log("No access");
		  }
		},
		err => {
		  //Connection failed message
		  console.log("No access");
		}
	);
	}
	getbonusdata()
	{
	this.authService.postData(this.userPostData, "getbonusdata").then(
		result => {
		  this.responsebonusData = result;
		  if (this.responsebonusData.bonusData) {
			this.bonusdata.id = this.responsebonusData.bonusData['id'];
			this.bonusdata.title = this.responsebonusData.bonusData['title'];
			 //console.log(this.bonusdata.id);
			 //console.log(this.bonusdata.title);
		  } else {
			console.log("No access");
		  }
		},
		err => {
		  //Connection failed message
		  console.log("No access");
		}
	);
	}
	goToOtherfanentryviewPage(id){
		this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
	}

	goToCompetescreenPage(id)
    {
			 
        this.navCtrl.push(CompetescreenPage,{ bonusId:id });
    }
	
	goToDailycompeteviewPage(id)
	{
		this.navCtrl.push(DailycompeteviewPage,{ superchallengeId: id });
	}

	goToHomePage()
    {
        this.navCtrl.push(HomePage);
    }

    goToexploreScreen()
    {
        this.navCtrl.push(ExplorePage);
    }

    goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

    goTofanRankingPage()
    {
        this.navCtrl.push(FanrankingPage);
    }

    goToprofilePage()
    {
        this.navCtrl.push(ProfilePage);
    }

}
