import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ExplorePage } from '../explore/explore';
import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';
import { FanrankingPage } from '../fanranking/fanranking';
import { ProfilePage } from '../profile/profile';
import { NotificationPage } from '../notification/notification';
import { AuthService } from '../../providers/authservice/authservice';
/**
 * Generated class for the FooterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-footer',
  templateUrl: 'footer.html',
})
export class FooterPage {

	// tab1Root = HomePage;
  // tab2Root = ExplorePage;
  // tab3Root = SuperfanchallengePage;
  // tab4Root = FanrankingPage;
	// tab5Root = ProfilePage;
	
	public userDetails: any;
	public resposeData: any;
	userPostData = {
		user_id: "",
		friend_id: "",
		type: "",
	};
	public notificationsCount: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService) {
		const data = JSON.parse(localStorage.getItem('userData'));
		this.userDetails = data.userData;
		this.userPostData.user_id = this.userDetails.user_id;
		this.getUserNotificationsData();
		setInterval(() => {
		   this.getUserNotificationsData();
		}, 60000);
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad FooterPage');
  }

	getUserNotificationsData() {
		this.authService.postData(this.userPostData, "getnotifications").then(
		result => {
		  this.resposeData = result;
		  if (this.resposeData.notificationsData) {
			this.notificationsCount = parseInt(this.resposeData.notificationsData.friend_request_count) + parseInt(this.resposeData.notificationsData.request_accepted_count) + parseInt(this.resposeData.notificationsData.request_rejected_count) + parseInt(this.resposeData.notificationsData.postlike_count) + parseInt(this.resposeData.notificationsData.postdislike_count) + parseInt(this.resposeData.notificationsData.postcomment_count) + parseInt(this.resposeData.notificationsData.unfriend_count);
			if(!this.notificationsCount){
				this.notificationsCount = 0;
			}
		  } else {
			console.log("error in notification.");
		  }
		},
		err => {
		  //Connection failed message
		}
	  );
	}
  goToHomePage()
    {
        this.navCtrl.push(HomePage);
    }

    goToexploreScreen()
    {
        this.navCtrl.push(ExplorePage);
    }

    goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

    goTofanRankingPage()
    {
        this.navCtrl.push(FanrankingPage);
    }

    goToprofilePage()
    {
        this.navCtrl.push(ProfilePage);
    }
	
	goToNotificationPage() {
        this.navCtrl.push(NotificationPage);
    }

}
