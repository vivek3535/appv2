import { AuthService } from './../../providers/authservice/authservice';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { UploadscreenPage } from '../uploadscreen/uploadscreen';
import { HomePage } from '../home/home';
/**
 * Generated class for the CompetescreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-competescreen',
  templateUrl: 'competescreen.html',
})
export class CompetescreenPage {
  public resposeData: any;
  
  bonusPostData = {
		bonus_id: "",
  };
  singlebonusPostData = {
    bonusid:"",
    bonustitle: "",
    bonusurl: "",
    type:"",
		bonusdescription: ""
  };
 
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService) {
    
    this.bonusPostData.bonus_id = navParams.get('bonusId');
     //console.log(this.bonusPostData.bonus_id);
     this.bonuspostdata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CompetescreenPage');
  }

	goToUploadscreenPage(ChallengeType, ChallengeID)
    {
     // console.log(this.ChallengeType);
    // console.log(UploadscreenPage, {postChallengeType : ChallengeType});
      this.navCtrl.push(UploadscreenPage, {postChallengeType : ChallengeType,postChallengebonusId:ChallengeID});
    }

    bonuspostdata()
    {
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
        cssClass: 'football_icon'
      });
      loading.present();
      this.authService.postData(this.bonusPostData, "bonuspostdata").then(
        result => {
          this.resposeData = result;
          if (this.resposeData.bonuscontentPostData) {
            //console.log(this.resposeData.bonuscontentPostData);
          loading.dismiss();
          this.singlebonusPostData.bonusid = this.resposeData.bonuscontentPostData.id;
          this.singlebonusPostData.bonustitle = this.resposeData.bonuscontentPostData.title;
          this.singlebonusPostData.bonusurl = this.resposeData.bonuscontentPostData.url;
          this.singlebonusPostData.type = this.resposeData.bonuscontentPostData.type;
          this.singlebonusPostData.bonusdescription = this.resposeData.bonuscontentPostData.description;
          } else {
          console.log("No access");
          loading.dismiss();
          }
        },
        err => {
          //Connection failed message
          loading.dismiss();
        }
      );
    }

    goTobackpage()
    {
      this.navCtrl.pop();
    }

	goToHomePage() {
        this.navCtrl.push(HomePage);
    }
}
