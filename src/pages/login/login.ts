
import { Component } from '@angular/core';
import { AuthService  } from '../../providers/authservice/authservice';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
/*import { HomescreenPage } from '../homescreen/homescreen';*/
import { ForgotpassPage } from '../forgotpass/forgotpass';
import { RegisterPage } from '../register/register';
import { TermsandconditionPage } from '../termsandcondition/termsandcondition';
import { FavteamPage } from '../favteam/favteam';
import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview';



@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  userDetails : any;
  resposeData : any;
  redirectStatus : any;
  PostId : any;
  userData = {"username":"", "password":"", "userzone":new Date().getTimezoneOffset()};

  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public authService: AuthService, public alertCtrl: AlertController) {
  }

  backtomain()
      {
          this.navCtrl.push(HomePage);
      }

      forGotpassPage()
      {
          this.navCtrl.push(ForgotpassPage);
      }

      favtmPage()
      {
          this.navCtrl.push(FavteamPage);
      }
      
    gotToSignUpPage()
    {
        this.navCtrl.push(RegisterPage);
    }

    gotToTermsandconditionPage()
    {
        this.navCtrl.push(TermsandconditionPage);
    }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad Login');
    const data = JSON.parse(localStorage.getItem('userData'));
    
    if(data){
      this.userDetails = data.userData;
		this.redirectStatus = "false";
		let name = "chalange=";
		let decodedCookie = decodeURIComponent(document.cookie);
		let ca = decodedCookie.split(';');
		for(let i = 0; i <ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				this.PostId = c.substring(name.length, c.length);
				this.redirectStatus = "true";
				document.cookie = "chalange=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
			}
		}
		
		if(this.redirectStatus == "true"){
			this.navCtrl.push(OtherfanentryviewPage,{ postId: this.PostId });
		}else{
			this.navCtrl.push(HomePage);
		}
	  //this.navCtrl.push(HomePage);
      // console.log(this.navCtrl.getActive().component.name);
      // console.log(this.navCtrl.getPrevious());
    }
  }

  login(){
   this.presentLoading();
   if(this.userData.username && this.userData.password){
    this.authService.postData(this.userData, "login").then((result) =>{
    this.resposeData = result;
    if(this.resposeData.userData){
		console.log(this.resposeData.userData);
		localStorage.setItem('userData', JSON.stringify(this.resposeData) )
		/*this.navCtrl.push(HomePage);*/
		this.redirectStatus = "false";
		let name = "chalange=";
		let decodedCookie = decodeURIComponent(document.cookie);
		let ca = decodedCookie.split(';');
		for(let i = 0; i <ca.length; i++) {
			let c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				this.PostId = c.substring(name.length, c.length);
				this.redirectStatus = "true";
				document.cookie = "chalange=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
			}
		}
		
		if(this.redirectStatus == "true"){
			this.navCtrl.push(OtherfanentryviewPage,{ postId: this.PostId });
		}else{
			this.navCtrl.push(HomePage);
		}
  }
  else{
    this.showAlert();
  }
    


    }, (err) => {
      //Connection failed message
    });
   }
   else{
    this.showAlert();
   }
  
  }

  presentLoading() {
    const loader = this.loadingCtrl.create({
      spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon',
      duration: 3000
    });
    loader.present();
  }



  showAlert() {
    const alert = this.alertCtrl.create({
      title: 'Incorrect!',
      subTitle: 'Email and Password',
      buttons: ['OK']
    });
    alert.present();
  }

}
