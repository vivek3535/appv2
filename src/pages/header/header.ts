import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { SuperfanchallengebonusPage } from '../superfanchallengebonus/superfanchallengebonus'; 
import { NotificationPage } from '../notification/notification';
import { AuthService } from '../../providers/authservice/authservice';
import { Login } from '../login/login';
import { HomePage } from '../home/home';
/**
 * Generated class for the HeaderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-header',
  templateUrl: 'header.html',
})
export class HeaderPage {

  public userDetails: any;
	public resposeData: any;
	userPostData = {
		user_id: "",
		friend_id: "",
		type: "",
	};
	public notificationsCount: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService) {
		const data = JSON.parse(localStorage.getItem('userData'));
		this.userDetails = data.userData;
		this.userPostData.user_id = this.userDetails.user_id;
		this.getUserNotificationsData();
		setInterval(() => {
		   this.getUserNotificationsData();
		}, 5000);
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad HeaderPage');
  }

  getUserNotificationsData() {
		this.authService.postData(this.userPostData, "getnotifications").then(
		result => {
		  this.resposeData = result;
		  if (this.resposeData.notificationsData) {
			this.notificationsCount = parseInt(this.resposeData.notificationsData.friend_request_count) + parseInt(this.resposeData.notificationsData.request_accepted_count) + parseInt(this.resposeData.notificationsData.request_rejected_count) + parseInt(this.resposeData.notificationsData.postlike_count) + parseInt(this.resposeData.notificationsData.postdislike_count) + parseInt(this.resposeData.notificationsData.postcomment_count) + parseInt(this.resposeData.notificationsData.unfriend_count) + parseInt(this.resposeData.notificationsData.tagged_count) + parseInt(this.resposeData.notificationsData.mention_count);
			if(!this.notificationsCount){
				this.notificationsCount = 0;
			}
		  } else {
			console.log("error in notification.");
		  }
		},
		err => {
		  //Connection failed message
		}
	  );
	}

	goToUploadPostImgVideoPage(){
	  this.navCtrl.push(SuperfanchallengebonusPage);
  }
  
  goToNotificationPage() {
    this.navCtrl.push(NotificationPage);
	}

	backToLogin() {
		this.navCtrl.push(Login);
	}
	logout() {
		localStorage.clear();
		setTimeout(() => this.backToLogin(), 1000);
	}

	goToHomePage() {
        this.navCtrl.push(HomePage);
    }
}
