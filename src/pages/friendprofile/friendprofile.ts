import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { AuthService } from '../../providers/authservice/authservice';

import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview';
import { HomePage } from '../home/home';
/**
 * Generated class for the FriendprofilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-friendprofile',
  templateUrl: 'friendprofile.html',
})
export class FriendprofilePage {

	public resposeData: any;
	public posts: string[] = [];
	userPostData = {
		user_id: "",
		page: 1,
		perPage: 15,
		totalData: 0,
		totalPage: 0,
	};
	public friendPostData = {
		user_id: "",
		friend_id: "",
		type: ""
	};
	userProfileData = {
		profilePicture: "",
		userName: "",
		username: "",
		userbio: "",
		userFriends: "",
		userFriendsCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
		getUserPointsRanks: "",
	};
	public userDetails: any;
	public friendStatus = 'false';
	public requestSentStatus = 'false';

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService: AuthService, public toastCtrl: ToastController, public alertCtrl: AlertController) {
		const data = JSON.parse(localStorage.getItem('userData'));
		this.userDetails = data.userData;
		this.userPostData.user_id = navParams.get('userId');
		this.getUserProfileData();
		console.log("id"+ this.userPostData.user_id);
		this.checkRequestSent();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriendprofilePage');
  }
  
	getUserProfileData() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getuserprofiledata").then(
		result => {
		  this.resposeData = result;
		  if (this.resposeData.profileData) {
			loading.dismiss();
			/*this.dataSet = this.resposeData.feedData;
			const dataLength = this.resposeData.feedData.length;
			this.userPostData.lastCreated = this.resposeData.feedData[
			  dataLength - 1
			].created;*/
			this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
			this.userProfileData.userName = this.resposeData.profileData.userName;
			this.userProfileData.username = this.resposeData.profileData.username;
			this.userProfileData.userFriends = this.resposeData.profileData.userFriends;
			this.userProfileData.userFriendsCount = this.resposeData.profileData.userFriendsCount;
			for(let i=0; i<this.resposeData.profileData.userPosts.length; i++) {
				this.posts.push(this.resposeData.profileData.userPosts[i]);
			}
			
			this.userPostData.page = this.resposeData.profileData.userPosts.page;
			this.userPostData.totalData = this.resposeData.profileData.userPosts.totalData;
			this.userPostData.totalPage = this.resposeData.profileData.userPosts.totalPage;
			
			//this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
			this.userProfileData.userbio = this.resposeData.profileData.userbio;
			this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
			this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
			this.userProfileData.getUserPointsRanks = this.resposeData.profileData.getUserPointsRanks;
			this.friendPostData.user_id = this.userDetails.user_id;
			this.friendPostData.friend_id = this.userPostData.user_id;

			let friendId = this.userDetails.user_id;
			let fstatus = 'false';
			Object.keys(this.userProfileData.userFriends).forEach(h => {
				if(this.userProfileData.userFriends[h].friend_id == friendId){
					fstatus = 'true';
				}
			});
			this.friendStatus = fstatus;
			console.log(this.friendStatus + "friendStatus");
			if(this.friendStatus == 'true'){
				this.friendPostData.type = "unfriends";
			}else{
				this.friendPostData.type = "friends";
			}
		  } else {
			console.log("No access");
			loading.dismiss();
		  }
		},
		err => {
		  //Connection failed message
		  loading.dismiss();
		}
	  );
	}
	
	respondFriendRequest(id, type){
		this.friendPostData.friend_id = id; 
		this.friendPostData.type = type;
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.friendPostData, "respondfriendrequest").then(
		result => {
			this.resposeData = result;
			
		  if (this.resposeData) {
				
				loading.dismiss();
			this.navCtrl.push(FriendprofilePage, { userId: id });
		  } else {
			console.log("No access");
			
		  }
		},
		err => {
		  //Connection failed message
		  loading.dismiss();
		}
	  );
	}
	
	checkRequestSent(){
		this.authService.postData(this.userDetails, "checkrequestsent").then(
		result => {
		  this.resposeData = result;
			if(this.resposeData.data.length){
				let currUserId = this.userPostData.user_id;
				let frsstatus = 'false';
				Object.keys(this.resposeData.data).forEach(h => {
					if(this.resposeData.data[h].friend_id == currUserId){
						frsstatus = 'true';
					}
				});
				this.requestSentStatus = frsstatus;
			}
		  },
		  err => {
		  //Connection failed message
		  }
		);
	}

	doInfinite(infiniteScroll) {
		setTimeout(() => {
			this.getUserProfileData();
			console.log('Async operation has ended');
			infiniteScroll.complete();
		}, 1000);
	}
	
	goToOtherfanentryviewPage(id){
		this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
	}

	goTobackpage()
	{
		this.navCtrl.pop();
	}
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }

}
