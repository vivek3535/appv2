import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController, ActionSheetController, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/authservice/authservice';

import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';
import { FriendprofilePage } from '../friendprofile/friendprofile';
import { DailycompeteviewPage } from '../dailycompeteview/dailycompeteview';   
import { CompetescreenPage } from '../competescreen/competescreen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HomePage } from '../home/home';
/**
 * Generated class for the OtherfanentryviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-otherfanentryview',
  templateUrl: 'otherfanentryview.html',
})
export class OtherfanentryviewPage {
	public resposeData: any;
	public tagFriendsData: any;
	fabentryPostData = {
		post_id: "",
	};
	singlePostData = {
		profilePicture: "",
		userId : "",
		username: "",
		userName: "",
		teamName: "",
		tags: "",
		tagsList: "",
		description: "",
		challenge_type: "",
		type: "",
		media: "",
		likes: "",
		likesStatus: "",
		share: "",
		comments: "",
		commentsCount: "",
		sfcchallengetitle:"",
	};

	userPostData = {
		user_id: "",
		feed_id: "",
		feed: "",
		postTags: "",
		mentionFriends: "",
		cmnt_parentID: 0,
		is_single: "true",
	};

	userProfileData = {
		profilePicture: "",
		userName: "",
		userbio: "",
		userFriends: "",
		userFollowers: "",
		userFollowersCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
		url: "",
		type: "",
		triviaGame: "",
		gameStatus: "",
		gameMessage: "",
	};

	gamePostData = {
		selectedOption: "",
		user_id: "",
		game_id: "",
	};
	userDetails: any;
	public bonusdata = {id:"",title:""};
	public superchallengedata = {id:"",title:""};
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService, public toastCtrl: ToastController, public alertCtrl: AlertController, public socialSharing: SocialSharing,  public actionsheetCtrl: ActionSheetController, public platform: Platform) {
		this.fabentryPostData.post_id = navParams.get('postId');
    this.singlePostImage();
		const data = JSON.parse(localStorage.getItem('userData'));
		this.userDetails = data.userData;
		this.userPostData.user_id = this.userDetails.user_id;
		this.gamePostData.user_id = this.userDetails.user_id;
		this.getUserProfileData();
		this.getsuperchallengedata();
		this.getbonusdata();
		
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtherfanentryviewPage');
  }

	getUserProfileData() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getuserprofiledata").then(
		result => {
			this.resposeData = result;
			if (this.resposeData.profileData) {
			loading.dismiss();
			/*this.dataSet = this.resposeData.feedData;
			const dataLength = this.resposeData.feedData.length;
			this.userPostData.lastCreated = this.resposeData.feedData[
				dataLength - 1
			].created;*/
			this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
			this.userProfileData.userName = this.resposeData.profileData.userName;
			this.userProfileData.userFriends = this.resposeData.profileData.userFriends;
			this.userProfileData.userFollowers = this.resposeData.profileData.userFollowers;
			this.userProfileData.userFollowersCount = this.resposeData.profileData.userFollowersCount;
			this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
			this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
			this.userProfileData.userbio = this.resposeData.profileData.userbio;
			this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
			this.userProfileData.triviaGame = this.resposeData.profileData.triviaGame;
			this.gamePostData.game_id = this.resposeData.profileData.triviaGame[0].id;
			this.userProfileData.gameStatus = this.resposeData.profileData.gameStatus;
			this.userProfileData.gameMessage = "You already played this game.";
			/*if(this.userProfileData.gameStatus == "Played"){
				let triviagameBgEl = document.querySelector('.triviagameBgParent');
				let boxtitleEl = document.querySelector('.boxtitle');
				triviagameBgEl.classList.add('triviagamedisplay');
				boxtitleEl.classList.add('showboxtitle');
				
				console.log(triviagameBgEl);
				console.log(boxtitleEl);
			}*/
			} else {
			console.log("No access");
			loading.dismiss();
			}
		},
		err => {
			//Connection failed message
			loading.dismiss();
		}
		);
	}
	
  singlePostImage(){
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.fabentryPostData, "getpostbyid").then(
			result => {
			  this.resposeData = result;
			  if (this.resposeData.postData) {
				loading.dismiss();
				this.userPostData.user_id = this.resposeData.postData.userId;
				this.singlePostData.profilePicture = this.resposeData.postData.profilePicture;
				this.singlePostData.username = this.resposeData.postData.username;
				this.singlePostData.userName = this.resposeData.postData.userName;
				this.singlePostData.userId = this.resposeData.postData.userId;
				this.singlePostData.teamName = this.resposeData.postData.teamName;
				this.singlePostData.tags = this.resposeData.postData.tags;
				this.singlePostData.tagsList = this.resposeData.postData.tagsList;
				this.singlePostData.description = this.resposeData.postData.description;
				this.singlePostData.challenge_type = this.resposeData.postData.challenge_type;
				this.singlePostData.type = this.resposeData.postData.type;
				this.singlePostData.media = this.resposeData.postData.media;
				this.singlePostData.likes = this.resposeData.postData.likes;
				this.singlePostData.likesStatus = this.resposeData.postData.likesStatus;
				this.singlePostData.share = this.resposeData.postData.share;
				this.singlePostData.comments = this.resposeData.postData.comments;
				this.singlePostData.commentsCount = this.resposeData.postData.commentsCount;
				this.singlePostData.sfcchallengetitle = this.resposeData.postData.sfcchallenge_title;
				if(this.singlePostData.challenge_type == "#1 Super Fan Challenge"){
					this.getsuperchallengedata();
				}else if(this.singlePostData.challenge_type == "Bonus Challenge"){
					this.getbonusdata();
				}
			  } else {
				console.log("No access");
				loading.dismiss();
			  }
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			}
		);
	}
	
	likePhoto(ID) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
	
		this.userPostData.feed_id = ID;
		this.authService.postData(this.userPostData, "likepost").then(
			result => {
				this.resposeData = result;
				loading.dismiss();
				if (this.resposeData.error <= 0) {
					this.presentToast(this.resposeData.message);
					this.singlePostData = this.resposeData.singlePostData;
					/*this.navCtrl.push(OtherfanentryviewPage,{ postId: ID });*/
				} else {
					this.presentToast("Error occurs while submitting like.");
				}
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			  this.presentToast("Error occurs while submitting like.");
			}
		);
			  
	}
	
	/* Delete User Post Starts */
	deletePost(ID) {
		let confirm = this.alertCtrl.create({
			title: 'Are you sure, you want to delete this post? There is NO undo!',
			message: '',
			buttons: [
			{
			  text: 'No',
			  handler: () => {
				console.log('Disagree clicked');
			  }
			}, {
			text: 'Yes',
			handler: () => {
				let loading = this.loadingCtrl.create({
					spinner: 'hide',
					content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
					cssClass: 'football_icon'
				});
				loading.present();
				this.userPostData.feed_id = ID;
				this.authService.postData(this.userPostData, "deletepost").then(
					result => {
						this.resposeData = result;
						loading.dismiss();
						if (this.resposeData.error == 0) {
							this.navCtrl.push(SuperfanchallengePage);
							this.presentToast("Post Deleted Successfully!");
						} else {
							this.presentToast("Error occurs while deleting post.");
						}
					},
					err => {
					  //Connection failed message
					  loading.dismiss();
					  this.presentToast("Error occurs while deleting post.");
					}
				);
			}
			}
		  ]
		});
	  confirm.present();
	}
	/* Delete User Post Ends */
	
	comments = [];
	addComment(ID, newComment: string) {
		if (newComment) {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			this.userPostData.user_id = this.userDetails.user_id;
			this.userPostData.feed_id = ID;
			this.userPostData.feed = newComment;
			this.authService.postData(this.userPostData, "addpostcomment").then(
				result => {
					loading.dismiss();
					this.resposeData = result;
					if (this.resposeData.error <= 0) {
						this.singlePostData.comments = this.resposeData.data;
						this.presentToast("Comment added successfully!");
						/*this.navCtrl.push(OtherfanentryviewPage,{ postId: ID });*/
					} else {
						this.presentToast("Error occurs while adding post comment.");
					}
				},
				err => {
				  //Connection failed message
				  loading.dismiss();
				  this.presentToast("Error occurs while adding post comment.");
				}
			);
		} 
	}


	addCommentReply(comment_id, ID,newReply: string) {
		if (newReply) {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			this.userPostData.user_id = this.userDetails.user_id;
			this.userPostData.cmnt_parentID = comment_id;
			this.userPostData.feed_id = ID;
			this.userPostData.feed = newReply;
			this.authService.postData(this.userPostData, "addpostcomment").then(
				result => {
					loading.dismiss();
					this.resposeData = result;
					if (this.resposeData.error <= 0) {
						this.singlePostData.comments = this.resposeData.data;
						this.presentToast("Replied successfully!");
						/*this.navCtrl.push(HomePage);*/
					} else {
						this.presentToast("Error occurs while adding post comment.");
					}
				},
				err => {
				  //Connection failed message
				  loading.dismiss();
				  this.presentToast("Error occurs while adding post comment.");
				}
			);
		} 
	}
	
	deletecomnt(index,commnt_id) {
		/* this.heroes.splice($commnt, 1); */
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		/*this.singlePostData.comments.splice(this.singlePostData.comments.indexOf(index),1);*/
		this.userPostData.feed_id = commnt_id;
		this.authService.postData(this.userPostData, "deletepostcomment").then(
			result => {
				loading.dismiss();
				this.resposeData = result;
				if (this.resposeData.error <= 0) {
					this.singlePostData.comments = this.resposeData.data;
					/*this.navCtrl.push(OtherfanentryviewPage,{ postId: this.fabentryPostData.post_id });*/
					this.presentToast("Comment deleted successfully!");
				} else {
					this.presentToast("Error occurs while adding post comment.");
				}
			},
			err => {
			  //Connection failed message
			  loading.dismiss();
			  this.presentToast("Error occurs while adding post comment.");
			}
		);
	}

	tagFriends(ID) {
		let alert = this.alertCtrl.create();
		alert.setTitle('Please Select Your Friends');
		let followersData = this.userProfileData.userFriends;
		Object.keys(followersData).forEach( function(key) {
			console.log(followersData[key].username);
			alert.addInput({
				type: 'checkbox',
				label: followersData[key].username,
				value: followersData[key].username,
			});
			
		});
		alert.addButton('Cancel');
		alert.addButton({
			text: 'Okay',
			handler: (data: any) => {
				this.tagFriendsData = "";
				data.forEach( d => {
					this.tagFriendsData  = this.tagFriendsData+d+",";
				});
				this.tagFriendsData = this.tagFriendsData;
				this.userProfileData.postTags = this.tagFriendsData;
				this.userPostData.postTags = this.tagFriendsData;
				this.singlePostData.tags = this.tagFriendsData;
				let loading = this.loadingCtrl.create({
					spinner: 'hide',
					content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
					cssClass: 'football_icon'
				});
				loading.present();
				this.userPostData.user_id = this.userDetails.user_id;
				this.userPostData.feed_id = ID;
				this.authService.postData(this.userPostData, "addposttags").then(
					result => {
						loading.dismiss();
						this.resposeData = result;
						if (this.resposeData.error <= 0) {
							this.presentToast("Tags added successfully!");
						} else {
							this.presentToast("Error occurs while adding post tags.");
						}
					},
					err => {
					  //Connection failed message
					  loading.dismiss();
					  this.presentToast("Error occurs while adding post tags.");
					}
				);
			}
		});

		alert.present();
	}

	mentionFriends($event, newComment) {
		let alert = this.alertCtrl.create();
		alert.setTitle('Please Select Your Friends');
		let followersData = this.userProfileData.userFriends;
		Object.keys(followersData).forEach( function(key)
 {
			alert.addInput({
				type: 'checkbox',
				label: followersData[key].username,
				value: followersData[key],
			});
			
		});
		alert.addButton('Cancel');
		alert.addButton({
			text: 'Okay',
			handler: (data: any) => {
				this.userPostData.mentionFriends = "";
				data.forEach( d => {
					$event.target.value  = $event.target.value+" "+d.username+" ";
					this.userPostData.mentionFriends = this.userPostData.mentionFriends+d.friend_id+",";
				});
				$event.target.value = $event.target.value.replace(/@(.*?) /i,"");
				newComment.value = $event.target.value;
			}
		});

		alert.present();
	}
	
  presentToast(msg) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: 6000,
			position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	getsuperchallengedata()
	{
		this.authService.postData(this.userPostData, "getsuperchallengedata").then(
			result => {
			  this.resposeData = result;
			  if (this.resposeData.superchallengeData) {
				this.superchallengedata.id = this.resposeData.superchallengeData.id;
				this.superchallengedata.title = this.resposeData.superchallengeData.title;
				console.log(this.resposeData.superchallengeData);
			  } else {
				console.log("No access 1");
			  }
			},
			err => {
			  console.log("No access");
			}
		);
	}
	getbonusdata()
	{
		this.authService.postData(this.userPostData, "getexploregamepostdata").then(
			result => {
			  this.resposeData = result;
			  if (this.resposeData.bonusData) {
				this.bonusdata.id = this.resposeData.bonusData['id'];
				this.bonusdata.title = this.resposeData.bonusData['title'];
			  } else {
				console.log("No access 2");
			  }
			},
			err => {
			  //Connection failed message
			  console.log("No access");
			}
		);
	}
	
	showReplies($event){
		let parentClass = $event.target.previousElementSibling.parentNode.parentNode.parentNode.className;
		let repliesEls = document.querySelectorAll('.'+parentClass+' li ul .replydeactive');
		let rstart;
		for (rstart = 0; rstart < repliesEls.length; rstart++) {
			repliesEls[rstart].classList.add('replyactive');
		}
		let activerepliesEls = document.querySelectorAll('.'+parentClass+' li ul .replydeactive.replyactive');
		for (rstart = 0; rstart < activerepliesEls.length; rstart++) {
			activerepliesEls[rstart].classList.remove('replydeactive');
		}
		$event.target.classList.remove('showmore');
		$event.target.classList.add('btndeactive');
	}
	
	showComments($event){
		let parentClass = $event.target.previousElementSibling.className;
		let commentsEls = document.querySelectorAll('.'+parentClass+' li.commentdeactive');
		let start;
		for (start = 0; start < commentsEls.length; start++) {
			commentsEls[start].classList.add('commentactive');
		}
		let activecommentsEls = document.querySelectorAll('.'+parentClass+' li.commentdeactive.commentactive');
		for (start = 0; start < activecommentsEls.length; start++) {
			activecommentsEls[start].classList.remove('commentdeactive');
		}
		$event.target.classList.remove('showmore');
		$event.target.classList.add('btndeactive');
	}
	
	goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

	goToFriendprofilePage(user_id)
    {
		this.navCtrl.push(FriendprofilePage,{ userId: user_id });
    }
	
	goToDailycompeteviewPage(id) {
		this.navCtrl.push(DailycompeteviewPage,{ superchallengeId: id });
	}
	goToCompetescreenPage(id) {
		this.navCtrl.push(CompetescreenPage,{ bonusId:id });
	}

	goTobackpage()
	{
		this.navCtrl.pop();
	}

	sharePostImage() {
		//this code is to use the social sharing plugin
		// message, subject, file, url
		this.socialSharing.share("Check this item:  appv2://home/")
		.then(() => {
			/*this.presentToast('testing');*/
			
		})
		.catch(() => {
	
		});
	}
	
	webSocialShareFn(id, url,element) {
		let actionSheet = this.actionsheetCtrl.create({
		  title: 'Share on Social Network',
		  cssClass: 'action-sheets-basic-page',
		  buttons: [
			{
			  text: 'Facebook',
			  role: 'share on facebook',
			  icon: !this.platform.is('ios') ? 'logo-facebook' : null,
			  handler: () => {
				this.shareOnFacebook(id, url);
			  }
			},
			{
			  text: 'Twitter',
			  role: 'share on twitter',
			  icon: !this.platform.is('ios') ? 'logo-twitter' : null,
			  handler: () => {
				this.shareOnTwitter(id);
			  }
			},
			{
			  text: 'Pinterest',
			  role: 'share on pinterest',
			  icon: !this.platform.is('ios') ? 'logo-pinterest' : null,
			  handler: () => {
				this.shareOnPinterest(id);
			  }
			},
			{
			  text: 'Email',
			  role: 'share on email',
			  icon: !this.platform.is('ios') ? 'mail' : null,
			  handler: () => {
				this.shareOnEmail(id);
			  }
			},
			{
			  text: 'Copy Link - http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+'',
			  role: 'Copy link',
			  icon: !this.platform.is('ios') ? 'ios-copy' : null,
			  cssClass: 'copyLinkTxt',
			  handler: () => {
			
				this.copyToClipboard(element);
				
			  }
			}
		  ]
		});
		actionSheet.present();
	}
		
	shareOnFacebook(id, url) {
		window.open('https://www.facebook.com/dialog/feed?app_id= 490648591450536 &redirect_uri=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+' &link='+url+' &picture=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id+' &caption=This%20is%20the%20caption &description=This%20is%20the%20description');
	}

	shareOnTwitter(id) {
		window.open('https://twitter.com/intent/tweet?url=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}

	shareOnPinterest(id) {
		window.open('http://pinterest.com/pin/create/link/?url=http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}
	
	shareOnEmail(id) {
		window.open('https://mail.google.com/mail/?view=cm&fs=1&to=&su=SFC Post Link&body='+ 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/chalanges.php/chalange/'+id);
	}

	copyToClipboard(element) {
		element.select();
		document.execCommand('copy');
		this.presentToast("Link has been copied!");
	}
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }

}
