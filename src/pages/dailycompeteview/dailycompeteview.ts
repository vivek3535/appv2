import { AuthService } from './../../providers/authservice/authservice';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { UploadscreenPage } from '../uploadscreen/uploadscreen';
import { HomePage } from '../home/home';
/**
 * Generated class for the DailycompeteviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-dailycompeteview',
  templateUrl: 'dailycompeteview.html',
})
export class DailycompeteviewPage {
  public resposeData: any;
  
  superchallengePostData = {
		superchallenge_id: "",
  };
  singlesuperchallengePostData = {
    superchallengeid:"",
    superchallengetitle: "",
    superchallengeurl: "",
    type:"",
		superchallengedescription: ""
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService,) {
    this.superchallengePostData.superchallenge_id = navParams.get('superchallengeId');
      //console.log(this.superchallengePostData.superchallenge_id);
      this.challengepostdata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DailycompeteviewPage');
  }
  
  challengepostdata() {
      let loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
        cssClass: 'football_icon'
      });
      loading.present();
      this.authService.postData(this.superchallengePostData, "challengepostdata").then(
        result => {
          this.resposeData = result;
          if (this.resposeData.superchallengecontentPostData) {
            //console.log(this.singlesuperchallengePostData.superchallengeid);
          loading.dismiss();
          this.singlesuperchallengePostData.superchallengeid = this.resposeData.superchallengecontentPostData.id;
          this.singlesuperchallengePostData.superchallengetitle = this.resposeData.superchallengecontentPostData.title;
          this.singlesuperchallengePostData.superchallengeurl = this.resposeData.superchallengecontentPostData.url;
          this.singlesuperchallengePostData.type = this.resposeData.superchallengecontentPostData.type;
          this.singlesuperchallengePostData.superchallengedescription = this.resposeData.superchallengecontentPostData.description;
          } else {
            console.log("No access");
            loading.dismiss();
          }
        },
        err => {
          //Connection failed message
          loading.dismiss();
        }
      );
    }
    
    goToUploadscreenPage(ChallengeType,ChallengeID)
    {
      //console.log(ChallengeType,ChallengeID);
        this.navCtrl.push(UploadscreenPage, {postChallengeType : ChallengeType,postChallengebonusId:ChallengeID});
    }


	goTobackpage()
	{
		this.navCtrl.pop();
	}
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }
}
