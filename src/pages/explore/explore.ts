import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, Platform, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/authservice/authservice';
import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview';

import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';
import { FanrankingPage } from '../fanranking/fanranking'; 


/**
 * Generated class for the ExplorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-explore',
  templateUrl: 'explore.html',
})
export class ExplorePage {
  explore: string = "exploresfc";
  public selectedgame: any;
  public selectedTeam:any; 
  public resposePostData: any;
  public exploredataSet: any;
  public exploregamedataSet: any;
  public teamdataSet:any;
  public gamedataSet:any;
  public team_id:any;
  public team_name:any;
  public game_id:any;
  public userDetails: any;
  public slidesPerView: any;
  userPostData = {
    user_id: "",
    team_id:"",
    game_id:"",
    team_name:"",
  };

  @ViewChild(Slides) slides: Slides;

    
    public showLeftButton: boolean;
    public showRightButton: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public authService:AuthService, public loadingCtrl: LoadingController, public platform: Platform) {
    const data = JSON.parse(localStorage.getItem('userData'));
     this.userDetails = data.userData;
     this.userPostData.user_id = this.userDetails.user_id;
     this.userPostData.team_id = this.userDetails.team_id;
     this.userPostData.game_id = this.userDetails.game_id;
     this.userPostData.team_name = this.userDetails.team_name;
     this.getteampostdata();
     this.getexplorepostdata();
     this.getgamepostdata();
     this.getexploregamepostdata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExploreScreenPage');
	if (this.platform.is('ios') || this.platform.is('android') || this.platform.is('iphone') || this.platform.is('tablet')) {
      // This will only print when on iOS
      this.slidesPerView = 1;
    }else {
      // This will only print when on iOS
      this.slidesPerView = 3;
    }
  }
  getexplorepostdata()
  {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
      cssClass: 'football_icon'
    });
    loading.present();
	this.authService.postData(this.userPostData, "getexplorepostdata").then(
    result => {
		loading.dismiss();
      this.resposePostData = result;
      if (this.resposePostData.exploreData !='') {
       this.exploredataSet = this.resposePostData.exploreData;
       //console.log(this.exploredataSet);
      } else {
        this.exploredataSet = [];
        console.log("No access");
        
      }
    },
    err => {
      //Connection failed message
	  console.log("out");
    }
  );
  }
  getteampostdata()
  {
   
	this.authService.postData(this.userPostData, "getteampostdata").then(
    result => {
      this.resposePostData = result;
      if (this.resposePostData.teamData) {
       this.teamdataSet = this.resposePostData.teamData;
       if(this.teamdataSet){
          this.selectedTeam = this.teamdataSet[0].id;
        }
      } else {
      
        console.log("No access");
      }
    },
    err => {
      //Connection failed message
	   console.log("out");
    }
  );
  }

  getteamsdata(id,team_name:any) 
  {
    this.selectedTeam = id;
     this.team_id = id;
      this.team_name = team_name;
      this.userPostData.team_id = this.team_id;
      this.userPostData.team_name = this.team_name;
      this.getexplorepostdata();
  }


  getgamepostdata()
  {
   
	this.authService.postData(this.userPostData, "getgamepostdata").then(
    result => {
      this.resposePostData = result;
      if (this.resposePostData.gameData) {
       this.gamedataSet = this.resposePostData.gameData;    
       if(this.gamedataSet){
        this.selectedgame = this.gamedataSet[0].id;
      }
      } else {
        console.log("No access");
      }
    },
    err => {
      //Connection failed message
	  console.log("out");
    }
  );
  }

  getgamesdata(id)
  {
    this.selectedgame = id;
      this.game_id = id;
      this.userPostData.game_id = this.game_id;
      this.getexploregamepostdata();
  }


  getexploregamepostdata()
  {
   
	this.authService.postData(this.userPostData, "getexploregamepostdata").then(
    result => {
      this.resposePostData = result;
      if (this.resposePostData.exploregameData !='') {
       this.exploregamedataSet = this.resposePostData.exploregameData;
       //console.log(this.exploregamedataSet);
      } else {
    //this.photos = "No Post Found!";
      this.exploregamedataSet = [];
        console.log("No access");
      }
    },
    err => {
      //Connection failed message
	  console.log("out");
    }
  );
  }

  goToOtherfanentryviewPage(id){
		this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
  }
  
  goToOtherfanentryviewPageexplore(id){
		this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
  }

  goToHomePage()
    {
        this.navCtrl.push(HomePage);
    }

    goToexploreScreen()
    {
        this.navCtrl.push(ExplorePage);
    }

    goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

    goTofanRankingPage()
    {
        this.navCtrl.push(FanrankingPage);
    }

    goToprofilePage()
    {
        this.navCtrl.push(ProfilePage);
    }

    slideChanged(): void {
		if (this.platform.is('ios') || this.platform.is('android') || this.platform.is('iphone') || this.platform.is('tablet')) {
			  let currentIndex = this.slides.getActiveIndex();
			  this.showLeftButton = currentIndex !== 0;
			  this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 1);
			  this.slidesPerView = 1;
		} else {
			  let currentIndex = this.slides.getActiveIndex();
			  this.showLeftButton = currentIndex !== 0;
			  this.showRightButton = currentIndex !== Math.ceil(this.slides.length() / 1);
			  this.slidesPerView = 3;
		}
	}

	  // Method that shows the next slide
	slideNext(): void {
	  if (this.platform.is('ios') || this.platform.is('android') || this.platform.is('iphone') || this.platform.is('tablet')) {
		  this.slides.slideNext();
	  } else {
		  this.slides.slideNext();
	  }
	}

	  // Method that shows the previous slide
	slidePrev(): void {
	  if (this.platform.is('ios') || this.platform.is('android') || this.platform.is('iphone') || this.platform.is('tablet')) {
		  this.slides.slidePrev();
	  } else {
		  this.slides.slidePrev();
	  }
	}
  

}
