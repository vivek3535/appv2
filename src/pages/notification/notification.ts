import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { AuthService } from '../../providers/authservice/authservice';
import { HomePage } from '../home/home';
import { FriendprofilePage } from '../friendprofile/friendprofile';
import { OtherfanentryviewPage } from '../otherfanentryview/otherfanentryview';
/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
	public userDetails: any;
	public notificationsCount: any;
	public userNotifications = {
		friend_request: "",
		timeline_notifications: "",
	};
	public resposeData: any;
	userPostData = {
		nid: "",
		user_id: "",
		friend_id: "",
		type: "",
	};
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService) {
	const data = JSON.parse(localStorage.getItem('userData'));
    this.userDetails = data.userData;
	this.userPostData.user_id = this.userDetails.user_id;
	this.getUserNotificationsData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NotificationPage');
  }
	
	getUserNotificationsData() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getnotifications").then(
		result => {
		  this.resposeData = result;
		  if (this.resposeData.notificationsData) {
			loading.dismiss();
			this.userNotifications.timeline_notifications = this.resposeData.notificationsData.timeline_notifications;
			this.userNotifications.friend_request = this.resposeData.notificationsData.friend_request;
		  } else {
			console.log("No access");
			loading.dismiss();
		  }
		},
		err => {
		  //Connection failed message
		  loading.dismiss();
		}
	  );
	}
	
	respondFriendRequest(i,$event, id, type){
		this.userPostData.friend_id = id;
		this.userPostData.type = type;
		$event.target.parentNode.parentNode.classList.add('hidenotification');
		this.authService.postData(this.userPostData, "respondfriendrequest").then(
		result => {
		  this.resposeData = result;
		  if(this.resposeData) {
			console.log("success");
		  } else {
			console.log("No access");
		  }
		},
		err => {
		  //Connection failed message
		  console.log("err");
		}
	  );
	}

	readNotification(id, $event, nid, type){
		this.userPostData.nid = nid;
		this.userPostData.type = type;
		$event.target.parentNode.parentNode.parentNode.classList.add('hidenotification');
		this.authService.postData(this.userPostData, "getnotifications").then(
		result => {
		  this.resposeData = result;
		  if(this.resposeData) {
				this.notificationsCount = parseInt(this.resposeData.notificationsData.friend_request_count) + parseInt(this.resposeData.notificationsData.request_accepted_count) + parseInt(this.resposeData.notificationsData.request_rejected_count) + parseInt(this.resposeData.notificationsData.postlike_count) + parseInt(this.resposeData.notificationsData.postdislike_count) + parseInt(this.resposeData.notificationsData.postcomment_count) + parseInt(this.resposeData.notificationsData.unfriend_count) + parseInt(this.resposeData.notificationsData.tagged_count) + parseInt(this.resposeData.notificationsData.mention_count);
					if(this.notificationsCount <= 0){
						this.navCtrl.push(HomePage);
					}
					console.log("success");
				} else {
			console.log("No access");
		  }
		},
		err => {
		  //Connection failed message
		  console.log("err");
		}
	  );
	}
	
	goToFriendprofilePage(user_id)
    {
		this.navCtrl.push(FriendprofilePage,{ userId: user_id });
    }
	
	goToOtherfanentryviewPage(id,nid, type)
    {
		this.userPostData.nid = nid;
		this.userPostData.type = type;
		this.authService.postData(this.userPostData, "getnotifications").then(
			result => {
				this.resposeData = result;
				if(this.resposeData) {
					console.log("success");
				} else {
					console.log("No access");
				}
			},
			err => {
				console.log("err");
			}
		);
        this.navCtrl.push(OtherfanentryviewPage,{ postId: id }); 
    }
	
	goTobackpage()
	{
		this.navCtrl.pop();
	}
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }

}
