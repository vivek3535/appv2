import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';

import { AuthService  } from '../../providers/authservice/authservice';
import { CommunityPage } from '../community/community';
import { FriendprofilePage } from '../friendprofile/friendprofile';
import { HomePage } from '../home/home';
/**
 * Generated class for the FriendsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
})
export class FriendsPage {
	public userDetails: any;
	public userPostData = {
		user_id: "",
		friend_id: "",
		type: "friends"
	};
	userProfileData = {
		profilePicture: "",
		userName: "",
		userFollowers: "",
		userFollowersCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
		url: "",
		type: "",
		triviaGame: "",
		gameStatus: "",
		gameMessage: "",
	};
	resposeData : any;
	users: any; 
	allusers: any;
	constructor(public navCtrl: NavController, public navParams: NavParams,public authService: AuthService,public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
		//this.ionViewDidLoad();
		const data1 = JSON.parse(localStorage.getItem('userData'));
		this.userDetails = data1.userData;
		this.userPostData.user_id = this.userDetails.user_id;
		console.log(this.userPostData.user_id + "121212221");
		this.getUserProfileData();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FriendsPage');
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getallusers").then(
			result => {
				this.resposeData = result;
				loading.dismiss();
			  if (this.resposeData.data) {
				this.users = this.resposeData.data;
				this.allusers = this.resposeData.data;
			  } else {
				console.log("No access");
			  }
			},
			err => {
			  //Connection failed message
				console.log("No access");
				loading.dismiss();
			}
		);
	}

	getUserProfileData() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getuserprofiledata").then(
		result => {
			this.resposeData = result;
			if (this.resposeData.profileData) {
			loading.dismiss();
			/*this.dataSet = this.resposeData.feedData;
			const dataLength = this.resposeData.feedData.length;
			this.userPostData.lastCreated = this.resposeData.feedData[
				dataLength - 1
			].created;*/
			this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
			this.userProfileData.userName = this.resposeData.profileData.userName;
			this.userProfileData.userFollowers = this.resposeData.profileData.userFollowers;
			this.userProfileData.userFollowersCount = this.resposeData.profileData.userFollowersCount;
			this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
			this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
			this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
			this.userProfileData.triviaGame = this.resposeData.profileData.triviaGame;
			this.userProfileData.gameStatus = this.resposeData.profileData.gameStatus;
			this.userProfileData.gameMessage = "You already played this game.";
			/*if(this.userProfileData.gameStatus == "Played"){
				let triviagameBgEl = document.querySelector('.triviagameBgParent');
				let boxtitleEl = document.querySelector('.boxtitle');
				triviagameBgEl.classList.add('triviagamedisplay');
				boxtitleEl.classList.add('showboxtitle');
				
				console.log(triviagameBgEl);
				console.log(boxtitleEl);
			}*/
			} else {
			console.log("No access");
			loading.dismiss();
			}
		},
		err => {
			//Connection failed message
			loading.dismiss();
		}
		);
	}
	
	addFriend(friendId){
		this.userPostData.friend_id = friendId;
		Object.keys(this.users).forEach(h => {
			if(this.users[h].id == friendId){
				this.users[h].checked = true;
			}
		});
		Object.keys(this.allusers).forEach(h => {
			if(this.allusers[h].id == friendId){
				this.allusers[h].checked = true;
			}
		});
		this.authService.postData(this.userPostData, "sendfriendrequest").then(
			result => {
			  this.resposeData = result;
			  if (this.resposeData.data == 1) {
				this.presentToast("Request Sent successfully!");
			  } else {
				this.presentToast("Error occurs.");
			  }
			},
			err => {
			  //Connection failed message
			  console.log("No access");
			}
		);
	}

	unFriend(id, type){
		this.userPostData.user_id = this.userDetails.user_id;
		this.userPostData.friend_id = id; 
		this.userPostData.type = type;
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "respondfriendrequest").then(
		result => {
			this.resposeData = result;
			if (this.resposeData) {
				loading.dismiss();
				this.presentToast("Unfriended Successfully");
				this.userPostData.type = 'friends';
				this.ionViewDidLoad();
			} else {
				console.log("No access");
				this.presentToast("Error occurs.");
			}
		},
		err => {
		  //Connection failed message
		  loading.dismiss();
		}
	  );
	}
	
	showAddFriends(){
		let usersListEl = document.querySelector('.searchbarlist');
		let usersListBtnEl = document.querySelector('.usersListBtn');
		usersListEl.classList.remove('opacityclass');
		usersListBtnEl.classList.add('build');
	}
	
	filterTechnologies(param: any): void {
		//this.ionViewDidLoad();
		this.users = this.allusers;
		let val: string = param;

		// DON'T filter the technologies IF the supplied input is an empty string
		if (val.trim() !== '') {
		  this.users = this.users.filter((item) => {
				return item.username.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.email.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.team.toLowerCase().indexOf(val.toLowerCase()) > -1;
		  })
		}
	}
	
	presentToast(msg) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: 6000,
			position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}
	
	goToCommunityPage(){
		this.navCtrl.push(CommunityPage); 
	}

	goTobackpage()
	{
		this.navCtrl.pop();
	}

	goToFriendprofilePage(user_id)
    {
		this.navCtrl.push(FriendprofilePage, { userId: user_id });
    }
	
	goToHomePage() {
        this.navCtrl.push(HomePage);
    }

}
