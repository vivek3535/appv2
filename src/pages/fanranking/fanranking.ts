import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/authservice/authservice';
import { FriendprofilePage } from '../friendprofile/friendprofile';

import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { ExplorePage } from '../explore/explore';
import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';

/**
 * Generated class for the FanrankingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-fanranking',
  templateUrl: 'fanranking.html',
})
export class FanrankingPage {
  public resposeData: any;
  public userDetails: any;
  public fanrankingdataSet:any;
  public fanrankingList:any;
  public resposePostData: any;
  userPostData = {
    user_id: "",
    
  };

  fabentryPostData = {
		post_id: "",
	};
  singlePostData = {
		profilePicture: "",
		userId : "",
		userName: "",
		teamName: "",
		challenge_type: "",
		type: "",
		media: "",
		likes: "",
		share: "",
		comments: "",
  };
  
  userProfileData = {
		profilePicture: "",
		userName: "",
		userFollowers: "",
		userFollowersCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
		url: "",
		type: "",
		triviaGame: "",
		gameStatus: "",
		gameMessage: "",
  };
  
  gamePostData = {
		selectedOption: "",
		user_id: "",
		game_id: "",
  };
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public authService:AuthService) {
    this.fabentryPostData.post_id = navParams.get('postId');
    const data = JSON.parse(localStorage.getItem('userData'));
     this.userDetails = data.userData;
     this.userPostData.user_id = this.userDetails.user_id;
     this.gamePostData.user_id = this.userDetails.user_id;
     this.getUserProfileData();

     this.getfanrankingedata();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FanrankingPage');
  }

 
  getfanrankingedata()
  {
    this.authService.postData(this.userPostData, "getfanrankingedata").then(
      result => {
        this.resposePostData = result;
        
        if (this.resposePostData.fanrankingData) {
         this.fanrankingdataSet = this.resposePostData.fanrankingData;
         this.fanrankingList = this.resposePostData.fanrankingData;
          console.log(this.fanrankingdataSet);
        } else {
      //this.photos = "No Post Found!";
          console.log("No access");
        }
      },
      err => {
        //Connection failed message
      console.log("out");
      }
    );
  }
 

  filterTechnologies(param: any): void {
    this.fanrankingdataSet = this.fanrankingList;
    let val: string = param;
   // alert(val);
    // DON'T filter the technologies IF the supplied input is an empty string
    if (val.trim() !== '') {
      this.fanrankingdataSet = this.fanrankingdataSet.filter((item) => {
        return item.username.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.userName.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.email.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.team_name.toLowerCase().indexOf(val.toLowerCase()) > -1;
      })
    }
  }

  getUserProfileData() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.authService.postData(this.userPostData, "getuserprofiledata").then(
		result => {
			this.resposeData = result;
			if (this.resposeData.profileData) {
			loading.dismiss();
			/*this.dataSet = this.resposeData.feedData;
			const dataLength = this.resposeData.feedData.length;
			this.userPostData.lastCreated = this.resposeData.feedData[
				dataLength - 1
			].created;*/
			this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
			this.userProfileData.userName = this.resposeData.profileData.userName;
			this.userProfileData.userFollowers = this.resposeData.profileData.userFollowers;
			this.userProfileData.userFollowersCount = this.resposeData.profileData.userFollowersCount;
			this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
			this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
			this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
			this.userProfileData.triviaGame = this.resposeData.profileData.triviaGame;
			this.gamePostData.game_id = this.resposeData.profileData.triviaGame[0].id;
			this.userProfileData.gameStatus = this.resposeData.profileData.gameStatus;
			this.userProfileData.gameMessage = "You already played this game.";
			/*if(this.userProfileData.gameStatus == "Played"){
				let triviagameBgEl = document.querySelector('.triviagameBgParent');
				let boxtitleEl = document.querySelector('.boxtitle');
				triviagameBgEl.classList.add('triviagamedisplay');
				boxtitleEl.classList.add('showboxtitle');
				
				console.log(triviagameBgEl);
				console.log(boxtitleEl);
			}*/
			} else {
			console.log("No access");
			loading.dismiss();
			}
		}, 
		err => {
			//Connection failed message
			loading.dismiss();
		}
		);
  }
  
  goToFriendprofilePage(user_id) {
    this.navCtrl.push(FriendprofilePage,{ userId: user_id });
	}
	
	goToHomePage()
    {
        this.navCtrl.push(HomePage);
    }

    goToexploreScreen()
    {
        this.navCtrl.push(ExplorePage);
    }

    goToSuperfanchallengePage()
    {
        this.navCtrl.push(SuperfanchallengePage);
    }

    goTofanRankingPage()
    {
        this.navCtrl.push(FanrankingPage);
    }

    goToprofilePage()
    {
        this.navCtrl.push(ProfilePage);
    }

}
