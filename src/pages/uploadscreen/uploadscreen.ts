import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';

import { AuthService } from '../../providers/authservice/authservice';
import { Camera, CameraOptions} from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { ProfilePage } from '../profile/profile';
import { Http } from '@angular/http';

import { HomePage } from '../home/home';
/**
 * Generated class for the UploadscreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-uploadscreen',
  templateUrl: 'uploadscreen.html',
})
export class UploadscreenPage {
	@ViewChild('fileInput') fileInput;
	public resposeData: any;
	public imageFileName: any;
	public userDetails: any;
	public userProfileData = {
		profilePicture: "",
		userName: "",
		userFollowers: "",
		userFollowersCount: "",
		userPosts: "",
		latestPost: "",
		userTeams: "",
		postMedia: "",
		postTags: "",
		postCheckedin: "",
		postText: "",
		postType: "",
	};
	userPostData = {
		user_id: "",
	};
	ChallengeType:any;
	ChallengebonusID:any;
	public selectedWebFiles: File = null;
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,public authService:AuthService,private camera : Camera,private transfer: FileTransfer,public toastCtrl: ToastController, public alertCtrl: AlertController, public http: Http) {
	const data = JSON.parse(localStorage.getItem('userData'));
	this.userDetails = data.userData;
	this.userPostData.user_id = this.userDetails.user_id;
	this.ChallengeType = navParams.get('postChallengeType');
	this.ChallengebonusID = navParams.get('postChallengebonusId');
	/*this.getUserProfileData();*/
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadscreenPage');
  }

  getUserProfileData() {
	let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
	});
	loading.present();
	this.authService.postData(this.userPostData, "getuserprofiledata").then(
	result => {
	  this.resposeData = result;
	  if (this.resposeData.profileData) {
		loading.dismiss();
		/*this.dataSet = this.resposeData.feedData;
		const dataLength = this.resposeData.feedData.length;
		this.userPostData.lastCreated = this.resposeData.feedData[
		  dataLength - 1
		].created;*/
		this.userProfileData.profilePicture = this.resposeData.profileData.profilePicture;
		this.userProfileData.userName = this.resposeData.profileData.userName;
		this.userProfileData.userFollowers = this.resposeData.profileData.userFollowers;
		this.userProfileData.userFollowersCount = this.resposeData.profileData.userFollowersCount;
		this.userProfileData.userPosts = this.resposeData.profileData.userPosts;
		this.userProfileData.latestPost = this.resposeData.profileData.latestPost;
		this.userProfileData.userTeams = this.resposeData.profileData.userTeams;
	  } else {
		console.log("No access");
		loading.dismiss();
	  }
	},
	err => {
	  //Connection failed message
	  loading.dismiss();
	}
  );
}
	
	/* Take user profile pic Starts */
	takePhoto() {
		if (Camera['installed']()) {
		const options : CameraOptions = {
			quality: 100, // picture quality
			mediaType: this.camera.MediaType.PICTURE,
			/*sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,*/
			allowEdit: true,
		}
		
		this.camera.getPicture(options) .then((imageData) => {
		  
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			/* Starts Preloader */
			/*this.presentToast(imageData);*/
			this.imageFileName = imageData.substring(imageData.lastIndexOf('/')+1);
			const fileTransfer: FileTransferObject = this.transfer.create();
			let options11: FileUploadOptions = {
				fileKey: 'file',
				fileName: this.imageFileName,
				params : {"userData":this.userDetails.user_id},
				headers: {}
			}
			fileTransfer.upload(imageData, 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/postsUpload.php', options11, true).then((data) => {
				loading.dismiss();
				this.resposeData = data;
				let mediaresponse = JSON.parse(this.resposeData.response);
				this.authService.postData(mediaresponse, "getreturndata").then(
					result => {
						this.resposeData = result;
						if(this.resposeData.postType != ""){
							this.userProfileData.postMedia = this.resposeData.postMedia;
							this.userProfileData.postType = this.resposeData.postType;
							this.presentToast("Upload Success");
						}
						else{
							this.presentToast("There was an error uploading the file, please try again!");
						}
					},
					err => {
						this.presentToast("Returned Error");
					}
				);
			}, (err) => {
				// error
				//alert("error"+JSON.stringify(err));
				loading.present();
				this.presentToast(JSON.stringify(err));
			});
		}, (err) => {
		  console.log(err);
		});
		}else {
			this.fileInput.nativeElement.click();
		}
	}
	/* Take user profile pic Ends */
	/* Upload user profile pic Starts */
	uploadPhoto() {
		if (Camera['installed']()) {
		const options : CameraOptions = {
			quality: 100, // picture quality
			mediaType: this.camera.MediaType.PICTURE,
			sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit: true,
		}
		this.camera.getPicture(options) .then((imageData) => {
			let loading = this.loadingCtrl.create({
				spinner: 'hide',
				content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
				cssClass: 'football_icon'
			});
			loading.present();
			/* Starts Preloader */
			this.imageFileName = imageData.substring(imageData.lastIndexOf('/')+1);
			const fileTransfer: FileTransferObject = this.transfer.create();
			let options11: FileUploadOptions = {
				fileKey: 'file',
				fileName: this.imageFileName,
				params : {"userData":this.userDetails.user_id},
				headers: {}
			}
			fileTransfer.upload(imageData, 'http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/postsUpload.php', options11, true).then((data) => {
				/* success */
				loading.dismiss();
				this.resposeData = data;
				let mediaresponse = JSON.parse(this.resposeData.response);
				this.authService.postData(mediaresponse, "getreturndata").then(
					result => {
						this.resposeData = result;
						if(this.resposeData.postType != ""){
							this.userProfileData.postMedia = this.resposeData.postMedia;
							this.userProfileData.postType = this.resposeData.postType;
							this.presentToast("Upload Success");
						}
						else{
							this.presentToast("There was an error uploading the file, please try again!");
						}
					},
					err => {
						this.presentToast("Returned Error");
					}
				);
			}, (err) => {
					this.presentToast(JSON.stringify(err));
			});
		}, (err) => {
		  console.log(err);
		});
		}else {
			this.fileInput.nativeElement.click();
		}
	}
	/* Upload user profile pic Ends */
	
	/* Create User Post Starts */
	createPost(){
		this.resposeData = {
			user_id: this.userPostData.user_id,
			postMedia : this.userProfileData.postMedia,
			postText: this.userProfileData.postText,
			postType: this.userProfileData.postType,
			team_id: this.userDetails.team_id,
			postChallengeType: this.ChallengeType,
			postChallengebonusId: this.ChallengebonusID
		};
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		if (this.ChallengebonusID) {
		loading.present();
		this.authService.postData(this.resposeData, "createpost").then(
			result => {
				this.resposeData = result;
				loading.dismiss();
			  if (this.resposeData.error <= 0) {
				this.presentToast(this.resposeData.message);
				this.userProfileData.latestPost = this.resposeData.latestPost;
				this.userProfileData.postText = null;
				this.userProfileData.postType = null;
				this.navCtrl.push(HomePage);
			  } else {
				console.log("No access");
				
				this.presentToast(this.resposeData.message);
			  }
			},
			err => {
			  loading.dismiss();
			}
		);
	}
	}
	/* Create User Post Ends */
	processWebImage(event){
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: '<img src="assets/imgs/ic_football2.gif" width="120" class="img-align" /> <span>Loading Please Wait...</span>',
			cssClass: 'football_icon'
		});
		loading.present();
		this.selectedWebFiles = <File>event.target.files[0];
		const fd = new FormData();
		fd.append("file", this.selectedWebFiles, this.selectedWebFiles.name);
		fd.append("userData", this.userDetails.user_id);
		//console.log('umesh');
		this.http.post('http://ec2-54-215-172-147.us-west-1.compute.amazonaws.com/sfc-app/api/webPostUpload.php', fd).map(res => res.json()).subscribe(data => {
			loading.dismiss();
			this.resposeData = data;
			if(this.resposeData.postType != ""){
				this.userProfileData.postMedia = this.resposeData.postMedia;
				this.userProfileData.postType = this.resposeData.postType;
				this.presentToast("Upload Success");
				event.target.value = "";
			}
			else{
				this.presentToast("There was an error uploading the file, please try again!");
			}
		}, error => {
			console.log(error);
			loading.dismiss();
		});
		
	}

	removePhoto(){
		this.userProfileData.postText = "";
		this.userProfileData.postType = "";
		this.userProfileData.postMedia = "";
	}
	
	postImageBtn(){
		this.navCtrl.push(ProfilePage);
	}
	
	presentToast(msg) {
		let toast = this.toastCtrl.create({
			message: msg,
			duration: 6000,
			position: 'bottom'
		});

		toast.onDidDismiss(() => {
			console.log('Dismissed toast');
		});

		toast.present();
	}

	goTobackpage()
	{
		this.navCtrl.pop();
	}

	goToHomePage() {
        this.navCtrl.push(HomePage);
    }
}
