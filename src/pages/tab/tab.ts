import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ExplorePage } from '../explore/explore';
import { SuperfanchallengePage } from '../superfanchallenge/superfanchallenge';
import { FanrankingPage } from '../fanranking/fanranking';
import { ProfilePage } from '../profile/profile';

/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {
  
  tab1Root = HomePage;
  tab2Root = ExplorePage;
  tab3Root = SuperfanchallengePage;
  tab4Root = FanrankingPage;
  tab5Root = ProfilePage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabPage');
  }

}
